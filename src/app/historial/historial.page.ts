import { Component, OnInit } from '@angular/core';
import { NavController, LoadingController, AlertController, MenuController  } from '@ionic/angular';
import { AuthService } from '../services/auth.service';
import { UserService } from '../services/user-service';


@Component({
  selector: 'app-historial',
  templateUrl: './historial.page.html',
  styleUrls: ['./historial.page.scss'],
})
export class HistorialPage implements OnInit {

  compras;
  isloading = true;

  constructor(private auth: AuthService,private user: UserService,public menuCtrl: MenuController, public nav: NavController) {}

  ngOnInit() {
  }

  ionViewWillEnter(){
    this.getProductos();
  }

  openMenu() {
    this.menuCtrl.toggle();
  }
  
  ayuda(){
    this.nav.navigateForward("contacto");
  }

  ver(id: any){
    localStorage.setItem("idcompra",id);
    this.nav.navigateForward("ver-compra");
  }

  getProductos() {
    this.user.getCurrentUser()
    .then(user => {
      this.auth.getGenKeyVal("Usuario","uid",user.uid).then(data => {
        let indata: any;
        indata = data;
        let iduser = indata[0].id;
        this.auth.getGenKeyVal("Compra","usuario",iduser).then(data => {
          this.compras = data;
          localStorage.setItem('Compras',JSON.stringify(data));
          console.log("compras: ",this.compras);
          this.isloading = false;
        })
      });
    });
  }

}
