import { Component, OnInit } from '@angular/core';
import { NavController, LoadingController, AlertController, MenuController  } from '@ionic/angular';
import { AuthService } from '../services/auth.service';
import { ServerService } from '../server/server.service';
import { UserService } from '../services/user-service';


@Component({
  selector: 'app-payload-checkout',
  templateUrl: './payload-checkout.page.html',
  styleUrls: ['./payload-checkout.page.scss'],
})
export class PayloadCheckoutPage implements OnInit {

  range;
  method = 'Selecciona...';
  methodtipo;
  nombredia = ['Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sábado', 'Domingo'];
  nombremethod = ['', 'Efectivo', 'Tarjeta', 'Datáfono'];
  propina: string = '0';
  bolsa = '0';
  otherpropina = 'Otro';
  subtotal = '0';
  envio = '0';
  cuotas = '1';
  total;
  valor_bolsa = '0';
  valor_bolsa_setting = 0;
  payload = {
    'metodoPago': '',
    'horarioRangoIni': 'dd/mm/aaaa hh:mm',
    'horarioRangoFin': 'dd/mm/aaaa hh:mm',
    'tarjetaId': '',
    'propina': '',
    'bolsa': '',
    'cvv': '',
    'rangoId': '0',
    'comentarios': '',
    'fechaSolicitud': 'dd/mm/aaaa hh:mm',
    'idDireccion': '',
    'uid': '',
    'total': '',
    'subtotal': '',
    'cupon_id': '',
    'cuotas': '1',
    'productos' : []
    };
 
  sending: boolean = false;

  constructor(public navCtrl: NavController,
    public menuCtrl: MenuController,
    private auth: AuthService,
    public server: ServerService,
    private user: UserService,
    private alertCtrl: AlertController) {
  }

  ngOnInit() {

  }

  ionViewWillEnter() {
    const dir = parseInt(localStorage.getItem('address'));

    // Get Range Time
    const rang = localStorage.getItem('range-time');
    const rangsplit = rang.split('|');
    console.log('Horas', rangsplit);
    const dia = new Date(rangsplit[0]);
    // let ini = new Date(rangsplit[1]).toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true });
    // let fini = new Date(rangsplit[2]).toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true });
    this.range = this.nombredia[dia.getDay()] + ' ' + rangsplit[1] + ' a ' + rangsplit[2];
    this.range = rangsplit[0] + ' ' + rangsplit[1] + ' a ' + rangsplit[2];

    // Get Method
    const methodactual = parseInt(localStorage.getItem('method'));
    if (!isNaN(methodactual)) {
      this.method = this.nombremethod[methodactual];
      if (methodactual == 2) {
        console.log("Nombre Tarjeta",localStorage.getItem("tarjeta_nombre"));
        if (localStorage.getItem("tarjeta_nombre") === 'undefined' || localStorage.getItem("tarjeta_nombre") === null) {
          this.method = "Selecciona...";
          this.methodtipo = "";
          localStorage.setItem('method',"")
        } else {
          const tarjetanombre = localStorage.getItem('tarjeta_nombre').split('|');
          this.method = tarjetanombre[0];
          this.methodtipo = tarjetanombre[1];
        } 
      }
    }

    // Get Propina
    const propinaactual = parseInt(localStorage.getItem('propina'));
    if (!isNaN(propinaactual)) {
       this.propina = String(propinaactual);
       if (propinaactual > 1000) {
         this.otherpropina = '$' + String(propinaactual);
         this.calculateTotal();
       }
    } else {
      this.propina = '1000';
      this.calculateTotal();
    }

    // Get Bolsa
    this.auth.getGenKeyVal('Settings', 'llave', 'valor_bolsa').then(data => {
      let indata: any;
      indata = data;
      this.valor_bolsa_setting = indata[0].valor;
      const bolsaactual = parseInt(localStorage.getItem('bolsa'));

      if (!isNaN(bolsaactual)) {
        this.bolsa = String(bolsaactual);

        if (String(this.bolsa) == '0') {
          console.log('1');
          this.valor_bolsa = '0';
          this.calculateTotal();
        } else {
          console.log('2');
          this.valor_bolsa = String(this.valor_bolsa_setting);
          this.calculateTotal();
        }
      }

    });
    this.calculateTotal();
  }

  calculateTotal() {
    // Set Subtotal, Envio, Total;
    this.subtotal = localStorage.getItem('subtotal');
    this.envio = localStorage.getItem('envio');
    this.total = parseInt(this.subtotal) + parseInt(this.envio) + parseInt(this.valor_bolsa) + parseInt(this.propina);
    console.log('sub' + parseInt(this.subtotal) + 'envio' + parseInt(this.envio) + 'bolsa' + this.valor_bolsa + 'propina' + parseInt(this.propina));
  }

  setRange() {
    this.navCtrl.navigateBack('payload-program');
  }

  setMethod() {
    this.navCtrl.navigateForward('tipo-pago');
  }

  setPropina(value: string) {
    this.propina = value;
    const propinacheck = parseInt(value);
    if (propinacheck > 1000) {
      this.presentAlertPropina();
    } else {
      this.otherpropina = 'Otro';
      localStorage.setItem('propina', value);
    }
    this.calculateTotal();
  }

  setBolsa(value: string) {
    this.bolsa = value;
    localStorage.setItem('bolsa', value);
    if (value == '0') {
      this.valor_bolsa = '0';
    } else {
      this.valor_bolsa = String(this.valor_bolsa_setting);
    }
    this.calculateTotal();
  }

  checkout() {
    this.sending = true;
    this.server.presentLoadingDefault('Realizando Pedido');
    this.calculateTotal();
    this.user.getCurrentUser()
    .then(user => {
      this.payload.uid = user.uid;
      this.payload.bolsa = this.bolsa;
      this.payload.propina = this.propina;
      this.payload.fechaSolicitud = new Date().toLocaleString('en-US');
      this.payload.idDireccion = localStorage.getItem('address');
      this.payload.tarjetaId = localStorage.getItem('tarjeta_actual');
      this.payload.subtotal = localStorage.getItem('subtotal');
      this.payload.total = this.total;
      this.payload.cvv = this.cuotas;
      this.payload.cuotas = this.cuotas;
      this.payload.metodoPago = localStorage.getItem('method');

      const rang = localStorage.getItem('range-time');
      const rangsplit = rang.split('|');
      console.log('Horas', rangsplit);
      const ini = rangsplit[0] + ' ' + rangsplit[1];
      const fini = rangsplit[0] + ' ' + rangsplit[2];

      let fechadia = new Date(rangsplit[0]);
      let dia = fechadia.getFullYear()+"-"+fechadia.getMonth()+"-"+fechadia.getDay();

      console.log("dia",dia);

      this.payload.horarioRangoIni = ini;
      this.payload.horarioRangoFin = fini;


      const cart = JSON.parse(localStorage.getItem('cart'));
      const numcart = JSON.parse(localStorage.getItem('numcart'));
      const unitcarttalla = JSON.parse(localStorage.getItem('unittallacart'));
      const preciocart = JSON.parse(localStorage.getItem('preciocart'));
      const maduracioncart = JSON.parse(localStorage.getItem('maduracioncart'));


      for (let i = 0; i < cart.length; i++) {
        const productocart = {};
        productocart['id'] = cart[i];
        productocart['numcart'] = numcart[i];
        productocart['unitcart'] = unitcarttalla[i];
        productocart['precio'] = preciocart[i];
        if (maduracioncart[i] == 'Verde') {
          productocart['maduracion_id'] = '1';
        } else if (maduracioncart[i] == 'Maduro') {
          productocart['maduracion_id'] = '3';
        } else {
          productocart['maduracion_id'] = '2';
        }
        this.payload.productos.push(productocart);
      }

      console.log('Log Pedido', this.payload);


      this.server.addPedido(this.payload).then((data) => {
        if (data['ok']) {
          this.server.dismissLoading();
          this.sending = true;
          this.payload.productos = [];
          this.navCtrl.navigateForward('payload-done');
        } else {
          this.payload.productos = [];
          this.server.showAlert('Error de Pedido', data['error_msg']);
          this.server.dismissLoading();
          this.sending = false;
        }

      }).catch((err) => {
          this.payload.productos = [];
          this.server.showAlert('Error de Pedido', 'Ha ocurrido un error al solicitar el pedido');
          this.server.dismissLoading();
          this.sending = false;
        });
    });
  }

  checkTarjeta() {
    const methodactual = parseInt(localStorage.getItem('method'));
    if (!isNaN(methodactual)) {
      if (methodactual == 2) { return false ; } else { return true; }
    }
  }

  goBack() {
    this.navCtrl.pop();
  }

  async presentAlertPropina() {
    const alert = await this.alertCtrl.create({
      header: 'Ingresa tu propina',
      inputs: [
      {
        name: 'otherpropina',
        placeholder: '$0'
      }
    ],
      buttons: [
      {
        text: 'Cancelar',
        role: 'cancelar',
        handler: data => {
          console.log('Cancel clicked');
          this.propina = '1000';
          localStorage.setItem('propina', '1000');
          this.calculateTotal();
        }
      },
      {
        text: 'Guardar',
        handler: data => {
          this.propina = data.otherpropina;
          this.otherpropina = '$' + data.otherpropina;
          localStorage.setItem('propina', data.otherpropina);
          this.calculateTotal();
        }
      }
      ]
    });

    await alert.present();
  }

}


