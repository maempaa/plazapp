import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders , HttpRequest } from '@angular/common/http';
import { UserService } from '../services/user-service';
import { LoadingController, AlertController } from '@ionic/angular';
import * as firebase from 'firebase/app';

import 'rxjs/add/operator/map';

@Injectable({
  providedIn: 'root'
})
export class ServerService {
  isLoading;
	loading;
  apipath = 'http://34.70.254.149/api/';
  registerUser = this.apipath + 'registrar-usuario';
  updateUsuario = this.apipath + 'update-usuario';
  agregarDireccion = this.apipath + 'add-direccion';
  agregarTarjeta = this.apipath + 'agregar-tarjeta';
  eliminarDireccion = this.apipath + 'remove-direccion';
  eliminarTarjeta = this.apipath + 'remove-tarjeta';
  newPedido = this.apipath + 'secure/pedido';
  terminos = this.apipath + 'terminos';

  constructor(public loadingCtrl: LoadingController,
    public http: HttpClient,
    public userService: UserService,
    public alertCtrl: AlertController) {}

  /* Crear Usuario */
  createNewUser(register: boolean, name: string, tel: string) {
    this.userService.getCurrentUser()
      .then(user => {
        let headers = new HttpHeaders();
        headers.append('Access-Control-Allow-Origin' , '*');
        headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
        headers.append('Accept', 'application/json');
        headers.append('content-type', 'application/json');
        let requestOptions =  { headers: headers };
        if (register) {
          user.name = 'Temporal';
        }
        if(user.email === null || user.email === undefined || user.email === "null"){
          user.email = user.uid+"@facebook.com";
        }
        this.http.post(this.registerUser, user, requestOptions)
          .subscribe(data => {
            if (register) {
              user.name = name; user.telefono = tel;
              this.updateUser(user);
            }
          }, error => {
          });
      }, (err) => {
      });
  }
  /* Actualizar Usuario */
  updateUser(user: any): any {
    return new Promise((resolve, reject) => {
      let headers = new HttpHeaders();
      headers.append('Access-Control-Allow-Origin' , '*');
      headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
      headers.append('Accept', 'application/json');
      headers.append('content-type', 'application/json');
      let requestOptions =  { headers: headers };
      this.http.post(this.updateUsuario, user, requestOptions).subscribe(data => {
        console.log(data);
        let userfb = firebase.auth().currentUser;
        userfb.updateProfile({
            displayName: user.name
        });
        localStorage.setItem('phoneUser', user.telefono);
        resolve(data);
      }, error => {
        console.log(error);
        reject(error.error);
      });
    }
    );
  }
  /* Agregar Dirección */
  addDireccion(direccion: { dir: string; adicional: string; predeterminada: number; lat: number; lng: number; uid: string; id: number; }): any {
    return new Promise((resolve, reject) => {
      let headers = new HttpHeaders();
      headers.append('Access-Control-Allow-Origin' , '*');
      headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
      headers.append('Accept', 'application/json');
      headers.append('content-type', 'application/json');
      let requestOptions =  { headers: headers };
      this.http.post(this.agregarDireccion, direccion, requestOptions)
      .subscribe(data => {
        console.log(data);
        resolve(data);
      }, error => {
        console.log(error);
        reject(error);
      });
    });
  }
  /* Eliminar Dirección */
  removeDireccion(datos: any) {
    return new Promise((resolve, reject) => {
      let headers = new HttpHeaders();
      headers.append('Access-Control-Allow-Origin' , '*');
      headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
      headers.append('Accept', 'application/json');
      headers.append('content-type', 'application/json');
      let requestOptions =  { headers: headers };
      this.http.post(this.eliminarDireccion, datos, requestOptions)
      .subscribe(data => {
        console.log(data);
        resolve(data);
      }, error => {
        console.log(error);
        reject(error);
      });
    });
  }
  /* Agregar Tarjeta */
  addTarjeta(datos: any) {
    return new Promise((resolve, reject) => {
      let headers = new HttpHeaders();
      headers.append('Access-Control-Allow-Origin' , '*');
      headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
      headers.append('Accept', 'application/json');
      headers.append('content-type', 'application/json');
      let requestOptions =  { headers: headers };
      this.http.post(this.agregarTarjeta, datos, requestOptions)
      .subscribe(data => {
        console.log(data);
        resolve(data);
      }, error => {
        console.log(error);
        reject(error);
      });
    });
  }
  /* Eliminar Tarjeta */
  removeTarjeta(datos: any) {
    return new Promise((resolve, reject) => {
      let headers = new HttpHeaders();
      headers.append('Access-Control-Allow-Origin' , '*');
      headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
      headers.append('Accept', 'application/json');
      headers.append('content-type', 'application/json');
      let requestOptions =  { headers: headers };
      this.http.post(this.eliminarTarjeta, datos, requestOptions)
      .subscribe(data => {
        console.log(data);
        resolve(data);
      }, error => {
        console.log(error);
        reject(error);
      });
    });
  }
  /* Agregar Pedido */
  addPedido(datos: any) {
    return new Promise((resolve, reject) => {
      let headers = new HttpHeaders();
      headers.append('Access-Control-Allow-Origin' , '*');
      headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
      headers.append('Accept', 'application/json');
      headers.append('content-type', 'application/json');
      let requestOptions =  { headers: headers };
      this.http.post(this.newPedido, datos, requestOptions)
      .subscribe(data => {
        console.log(data);
        resolve(data);
      }, error => {
        console.log(error);
        reject(error);
      });
    });
  }
  /* Obtener Términos */
  getTerminos() {
    return new Promise((resolve, reject) => {
      let headers = new HttpHeaders();
      headers.append('Access-Control-Allow-Origin' , '*');
      headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
      headers.append('Accept', 'application/json');
      headers.append('content-type', 'application/json');
      let requestOptions =  { headers: headers };
      const datos = {};
      this.http.post(this.terminos, datos, requestOptions)
      .subscribe(data => {
        console.log(data);
        resolve(data);
      }, error => {
        console.log(error);
        reject(error);
      });
    });
  }
  /* Geolocalizar */
  geoCode(dir: string): any {
    return new Promise((resolve, reject) => {
      dir = dir.replace(/[\W_]+/g,' ');
      dir = dir.replace(/ /g, "+", );
      let url = 'https://maps.googleapis.com/maps/api/geocode/json?address='+ dir + "&key=AIzaSyCpXAChhUJHccLmv3sE-FM8enJ79lkS4F0";
      console.log('url', url);
      this.http.get(url).subscribe(data => {
        console.log(data);
        resolve(data);
      }, error => {
        console.log(error);
        reject(error.error);
      });
    }
    );
  }
  /* Mostrar Carga */
  async presentLoadingDefault(title: string) {
    this.loading = await this.loadingCtrl.create({
      spinner: 'circles',
      message: title,
      translucent: true,
      cssClass: 'custom-class custom-loading'
    });
    return await this.loading.present();
  }
  /* Mostrar Alerta */
  async showAlert(titulo, mensaje) {
    const alertct = await this.alertCtrl.create({
      header: titulo,
      message: mensaje,
      buttons: ['OK']
    });
    await alertct.present();
  }
  /* Descartar Carga */
  async dismissLoading() {
    setTimeout(async () => {
      return await this.loading.dismiss();
    }, 500);

  }
  /* Funcion Validar Email */
  validateEmail(email) {
    let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

}
