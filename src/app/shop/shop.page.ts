import { Component, OnInit, ViewChild } from '@angular/core';
import { NavController, LoadingController, AlertController, MenuController  } from '@ionic/angular';
import { ParametrosService } from '../services/parametros.service';

import { AuthService } from '../services/auth.service';
import { AngularFireDatabase } from '@angular/fire/database';
import { Observable } from 'rxjs/Observable';
import { IonContent } from '@ionic/angular';

@Component({
  selector: 'app-shop',
  templateUrl: './shop.page.html',
  styleUrls: ['./shop.page.scss'],
})
export class ShopPage implements OnInit {
  @ViewChild(IonContent, null) content: IonContent;
  categoria: any;
  categoriaid: any;
  tempproductos: any;
  productos: any;
	sliderConfig = {
	  slidesPerView: 2,
	  spaceBetween: 10,
	  centeredSlides: false
  };

  constructor(public navCtrl: NavController,public param: ParametrosService,private auth: AuthService,public db: AngularFireDatabase,public menuCtrl: MenuController) {}

  ngOnInit() {}

  scrollToTop() {
    this.content.scrollToTop(400);
  }

  ionViewWillEnter(){
    this.scrollToTop();
    this.categoriaid = parseInt(localStorage.getItem("cat-select"));
    console.log("Recibiendo Categoria: ", this.categoriaid);
    this.categoria = JSON.parse(localStorage.getItem('categorias'));
    this.categoria = this.categoria.filter(item => item.id === this.categoriaid)[0];
    this.tempproductos = JSON.parse(localStorage.getItem('productos'));
    this.tempproductos = this.tempproductos.filter(item => item.categoria === this.categoriaid);
    this.productos = this.tempproductos;
    localStorage.setItem("fromSearch","false");
  }

  selectProducto(id: any){
    localStorage.setItem("product-select",id);
    this.navCtrl.navigateForward("addproducto");
  }

  productoAgregado(id: any){
    let cart = JSON.parse(localStorage.getItem('cart')); 
    if (cart.indexOf(id) == -1) {
      return false;
    } else {
      return true;
    }
  }

  openMenu() {
    this.menuCtrl.toggle();
  }

  goBack() {
    this.navCtrl.navigateBack("tabs/home");
  }

}
