import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { Events, IonContent, NavController } from '@ionic/angular';
import { ChatService, ChatMessage, UserInfo } from '../services/chat.service';


import { distanceInWordsToNow } from 'date-fns'

import * as es from 'date-fns/locale/es';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.page.html',
  styleUrls: ['./chat.page.scss'],
})
export class ChatPage implements OnInit {

  @ViewChild(IonContent, null) content: IonContent;
  @ViewChild('chat_input', null) messageInput: ElementRef;
  msgList: ChatMessage[] = [];
  user: UserInfo;
  toUser: UserInfo;
  editorMsg = '';
  showEmojiPicker = false;
  status: String;

  constructor(private chatService: ChatService,
              private events: Events,
              private navCtrl: NavController) {
    
  }

  ngOnInit() {
  }

  ionViewWillLeave() {
    /*// unsubscribe
    this.events.unsubscribe('chat:received');*/
  }

  ionViewDidEnter() {
   /* //get message list
    this.getMsg();

    // Subscribe to received  new message events
    this.events.subscribe('chat:received', msg => {
      this.pushNewMsg(msg);
    })*/


    this.status = this.chatService.CHAT_STATUS.NO_INICIADO;
    // Get the navParams toUserId parameter

    this.toUser = {
      id: "agente007",
      name: "usuario"
    };

    console.log(this.toUser);
    // Get mock user information
    this.chatService.getUserInfo()
    .then((res) => {
      this.user = res
      console.log("user chat",res);
    });

    this.getMsg();
    // Subscribe to received  new message events
    this.events.subscribe('chat:received', msg => {
      this.pushNewMsg(msg);
    })
    // Subscribe to connection update events
    this.events.subscribe('chat:connection_update', status => {
        this.status = status;
        console.log("Conectado?",this.status);
    })
    
   this.chatService.inciarChat();
   this.scrollToBottom();
  }

  onFocus() {
    this.showEmojiPicker = false;
    //this.content.resize();
    this.scrollToBottom();
  }

  switchEmojiPicker() {
    this.showEmojiPicker = !this.showEmojiPicker;
    if (!this.showEmojiPicker) {
      this.focus();
    } else {
      this.setTextareaScroll();
    }
    //this.content.resize();
    this.scrollToBottom();
  }

  /**
   * @name getMsg
   * @returns {Promise<ChatMessage[]>}
   */
  getMsg() {
    // Get mock message list
    return this.chatService
    .getMsgList()
    .subscribe(res => {
      this.msgList = res;
      this.scrollToBottom();
    });
  }

  /**
   * @name sendMsg
   */
  sendMsg() {
    if (!this.editorMsg.trim()) return;

    // Mock message
    const id = Date.now().toString();
    let newMsg: ChatMessage = {
      messageId: Date.now().toString(),
      userId: this.user.id,
      userName: 'Tú' /*this.user.name*/,
      userAvatar: this.user.avatar,
      toUserId: this.toUser.id,
      time: Date.now(),
      message: this.editorMsg,
      status: 'pending'
    };

    this.pushNewMsg(newMsg);
    this.editorMsg = '';

    if (!this.showEmojiPicker) {
      this.focus();
    }

    this.chatService.sendMsg(newMsg)
    .then(() => {
      let index = this.getMsgIndexById(id);
      if (index !== -1) {
        this.msgList[index].status = 'success';
      }
    })
  }

  /**
   * @name pushNewMsg
   * @param msg
   */
  pushNewMsg(msg: ChatMessage) {
    console.error("Pushing: ", msg);
    const userId = this.user.id,
      toUserId = this.toUser.id;
    // Verify user relationships
    if (msg.userId === userId && msg.toUserId === toUserId) {
      this.msgList.push(msg);
    } else if (msg.toUserId === userId && msg.userId === toUserId) {
      this.msgList.push(msg);
    }
    this.scrollToBottom();
  }

  getMsgIndexById(id: string) {
    return this.msgList.findIndex(e => e.messageId === id)
  }

  scrollToBottom() {
    setTimeout(() => {
      if (this.content.scrollToBottom) {
        this.content.scrollToBottom();
      }
    }, 400)
  }

  private focus() {
    if (this.messageInput && this.messageInput.nativeElement) {
      this.messageInput.nativeElement.focus();
    }
  }

  private setTextareaScroll() {
    const textarea =this.messageInput.nativeElement;
    textarea.scrollTop = textarea.scrollHeight;
  }

  timeago(value: string){
  	return distanceInWordsToNow(new Date(value), { addSuffix: true, locale:es });
  }

  goBack() {
    this.navCtrl.pop();
  }
  
}
