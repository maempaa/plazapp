import { Component, OnInit } from '@angular/core';
import { NavController, LoadingController, AlertController, MenuController  } from '@ionic/angular';

import { AuthService } from '../services/auth.service';
import { ServerService } from '../server/server.service';
import { UserService } from '../services/user-service';


@Component({
  selector: 'app-micuenta',
  templateUrl: './micuenta.page.html',
  styleUrls: ['./micuenta.page.scss'],
})
export class MicuentaPage implements OnInit {
	user;
  userimg;
  name;
  tel;
  email;
  pass;
  social;
  constructor(private auth: AuthService,
    public menuCtrl: MenuController,
    public server: ServerService,
    public nav: NavController,
    public userService: UserService,
    public navCtrl: NavController) {
  	this.userService.getCurrentUser()
    .then(user => {
    this.name = user.name;
    this.email = user.email;
    this.tel = user.telefono;
    this.userimg = user.image;
    if (user.provider =='facebook.com') {
      this.social = 'facebook';
      this.pass = 'social';
    } else if (user.provider == "google.com") {
      this.social = 'google';
      this.pass = 'social';
    } else {
      this.social = 'none';
      this.pass = '1234';
    }
    console.log(user);
    });
  }

  ngOnInit() {}

  ionViewWillEnter() {
    localStorage.setItem('fromCart', "false");
  }

  actualizarUsuario() {
    console.log('Actualizando Usuario');
    this.userService.getCurrentUser()
    .then(user => {
      if (user.provider == "facebook.com" || user.provider =='google.com') {
        user.telefono = this.tel;
      } else {
        user.telefono = this.tel;
        user.name = this.name;
      }
      console.log(user);
      this.server.updateUser(user).then((data) => {
        this.server.showAlert('Usuario Actualizado', "Usuario actualizado con éxito");
      }).catch((err) => {
        this.server.showAlert('Error al Actualizar','Ha ocurrido un error al actualizar la información del usuario ');
      });
    });
  }

  openMenu() {
    this.menuCtrl.toggle();
  }

  goTarjetas() {
    this.nav.navigateForward('tarjetas');
  }

  logout() {
    this.auth.signOut();
    this.nav.navigateRoot('intro');
  }

  gotorecovery() {
    this.nav.navigateBack('repass');
  }

  fbLogin() {
    console.log('Facebook Login Reconnect');
    this.auth.signOut();
    this.auth.doFacebookLogin().then((resp) => {
      console.log('Autenticado:', resp);
      this.navCtrl.navigateRoot('tabs/home');
    }).catch((err) => {
      this.server.showAlert('Error al Ingresar','Error al ingresar con Facebook, intente nuevamente.');
      console.log('error:', err);
    });
  }

  googleLogin() {
    console.log('Google Login Reconnect');
    this.auth.signOut();
    this.auth.doGoogleLogin().then((resp) => {
      console.log('Autenticado:', resp);
        this.navCtrl.navigateRoot('tabs/home');
      }).catch((err) => {
      this.server.showAlert('Error al Ingresar', "Error al ingresar con Google, intente nuevamente.");
      console.log('error:', err);
    });
  }



}
