payload = {
	"metodoPago": 2,
	"rangoId": 1, 
    "idTarjeta": "1",
    "propina": 1,
    "bolsa", 1,
    "comentarios": "Comentario",
    "fechaSolicitud": "12-04-2019 13:00",
    "idDireccion": "1",
    "email": "odproductor@hotmail.com",
    "uid": "n5jGaIAcDDdWpgk5fkdD7MV6pZC2",
    "total": 10000,
    "subtotal": 7000,
    "costoEnvio": 3000, 
    "telefono": "3183506921",
    "cupon_id":0,
    "productos" : [
    	{
    		"id": 1,
    		"numcart": 1,
    		"unitcart": 1,
    		"precio": 1000,
    		"estado": 0
    	},
    	{
    		"id": 2,
    		"numcart": 1,
    		"unitcart": 1,
    		"precio": 2000,
    		"estado": 1
    	},
    	{
    		"id": 3,
    		"numcart": 1,
    		"unitcart": 1,
    		"precio": 3000,
    		"estado": 2
    	}
    ]
};