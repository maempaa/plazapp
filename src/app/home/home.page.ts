import { Component } from '@angular/core';
import { Platform } from '@ionic/angular';
import { NavController, LoadingController, AlertController, MenuController  } from '@ionic/angular';

import { AuthService } from '../services/auth.service';
import { AngularFireDatabase } from '@angular/fire/database';
import { Observable } from 'rxjs/Observable';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
import { ParametrosService } from '../services/parametros.service';
import { ServerService } from '../server/server.service';
import { EventEmitterService } from "./../services/events/event-emitter.service";

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})

export class HomePage {
  categorias: any;
  productos: any;
  isloading = true;
  urlimagen = 'http://34.70.254.149/uploads/images/galeria/';
  sliders: any;
  user: any;
  nologin = false;

  constructor(
    private screenOrientation: ScreenOrientation,
    public navCtrl: NavController,
    public alertCtrl: AlertController,
    private auth: AuthService,
    public db: AngularFireDatabase,
    public loadingCtrl: LoadingController,
    public menuCtrl: MenuController,
    private param: ParametrosService,
    public platform: Platform,
    private server: ServerService,
    public event: EventEmitterService
  ) {
    this.menuCtrl.enable(true, 'mimenu');
    if (this.platform.is('cordova') ) {
      //this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
    }
    this.user = this.auth.getUser();
    console.log('user...', this.user);
    if (this.user === null) {
        this.nologin = true;
    }
    this.getSliders();
    this.isloading = true;
    this.getCategorias();
    this.isloading = true;
    this.getProductos();
    let cart = JSON.parse(localStorage.getItem('cart'));
    if (cart === undefined || cart === null || cart.length === 0) {
      cart = [];
      const numcart = [];
      const unitcart = [];
      const unittallacart = [];
      const badgecart = '0';
      const preciocart = [];
      const maduracioncart = [];
      localStorage.setItem('cart', JSON.stringify(cart));
      localStorage.setItem('numcart', JSON.stringify(numcart));
      localStorage.setItem('unitcart', JSON.stringify(unitcart));
      localStorage.setItem('unittallacart', JSON.stringify(unittallacart));
      localStorage.setItem('badgecart', badgecart);
      localStorage.setItem('preciocart', JSON.stringify(preciocart));
      localStorage.setItem('maduracioncart', JSON.stringify(maduracioncart));
      console.log('Cart is created', cart);
    } else {
      console.log('Cart already created', cart);
    }

    this.event.sendEmit();
  }

  ionViewWillEnter() {
    localStorage.setItem('fromCart', 'false');
    localStorage.setItem('fromSearch', 'false');
  }


  bienvenido() {
    this.navCtrl.navigateForward('/bienvenido');
  }

  intro() {
    this.navCtrl.navigateForward('/intro');
  }

  goCarrito() {
    if (!this.nologin) {
    this.navCtrl.navigateForward('/cart');
    } else{
      this.nologinAlert();
    }
  }

  goReg() {
    this.navCtrl.navigateForward('/registro');
  }

  goLog() {
    this.navCtrl.navigateForward('/log');
  }

  goShop(id: any) {
    console.log('nologin', this.nologin);
    if (!this.nologin) {
    localStorage.setItem('cat-select', id);
    this.navCtrl.navigateForward('tabs/shop');
    } else {
      this.nologinAlert();
    }
  }

  ionViewDidLoad() {
    console.log('Home Page');
  }


  getCategorias() {
    const me = this;
    this.auth.getGenOV('Categoria').then(data => {
      me.categorias = data;
      localStorage.setItem('categorias', JSON.stringify(data));
      console.log('Categorias: ', me.categorias);
      this.isloading = false;
    });
  }

  getSliders() {
    this.auth.getGenKeyVal('Imagengaleria', 'galeria', 1).then(data => {
      this.sliders = data;
      console.log('Sliders: ', this.sliders);
    });
  }

  getProductos() {
    const me = this;
    this.auth.getGenOV('Producto').then(data => {
      me.productos = data;
      localStorage.setItem('productos', JSON.stringify(data));
      console.log('Productos: ', me.productos);
      this.isloading = false;
    });
  }

  openMenu() {
    this.menuCtrl.toggle();
  }

  nologinAlert() {
    this.server.showAlert('', 'Para acceder debes estar registrado o logueado.');
  }


}
