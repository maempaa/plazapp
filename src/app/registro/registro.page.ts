import { Component, OnInit } from '@angular/core';
import { NavController, LoadingController, AlertController, MenuController  } from '@ionic/angular';
import { AuthService } from '../services/auth.service';
import { AngularFireDatabase } from '@angular/fire/database';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/combineLatest';
import { ServerService } from '../server/server.service';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.page.html',
  styleUrls: ['./registro.page.scss'],
})
export class RegistroPage implements OnInit {
  alert: any;
  email: string = "";
  name: string = "";
  lastname: string = "";
  tel: string = "";
  password: string = "";

  constructor(public navCtrl: NavController,public menuCtrl: MenuController,  
    private auth: AuthService, public alertCtrl: AlertController, db: AngularFireDatabase,private server: ServerService) {}

  ngOnInit() {}

  /* Ir a login */
  goLog(){
    this.navCtrl.navigateForward('log');
  }
  /* Ir a terminos */
  goTerminos(){
    this.navCtrl.navigateForward('terminos');
  }
  /* Ir a Intro */
  goIntro(){
    this.navCtrl.navigateBack('intro');
  }
  /* Registro Email */
  signup() {
    let error: string = '';
    let hay_error : boolean = false;
    if(this.password.length < 4){ error = 'Debes ingresar una contraseña de al menos 4 caracteres'; hay_error = true;}
    if(!this.server.validateEmail(this.email)){ error = 'Email inválido'; hay_error = true; }
    if(this.tel.length < 7){ error = 'El número de teléfono debe tener más de 6 números'; }
    if(this.name.length < 3){ error = 'Debes ingresar un nombre de al menos 3 caracteres'; hay_error = true;}
    if(this.lastname.length < 3){ error = 'Debes ingresar un apellido de al menos 3 caracteres'; hay_error = true;}

    if(hay_error){
      this.server.showAlert('Error de Registro',error);
      return;
    }else{
      let NombreCompleto = this.name +" "+this.lastname;
      console.log("error", NombreCompleto);
      this.server.presentLoadingDefault("Registrando...");
      let datos = {
        email: this.email,
        password: this.password,
        username: NombreCompleto,
        tel: this.tel
      };
      this.auth.doRegister(datos).then((resp)=>{
        this.server.dismissLoading();
        this.navCtrl.navigateRoot("tabs/home");
      }).catch((err)=>{
        this.server.dismissLoading();
        this.server.showAlert('Error de Registro',"Verifica tus datos e intenta nuevamente!");
      });

    }
  }
  /* Registro Facebook */
  fbLogin(){
    this.server.presentLoadingDefault("Ingresando desde Facebook");
    this.auth.doFacebookLogin().then((resp)=>{
      this.server.dismissLoading();
      this.navCtrl.navigateRoot("home")
    }).catch((err)=>{
      this.server.dismissLoading();
      this.server.showAlert("Error al Ingresar","Error al ingresar con Facebook, intente nuevamente.");
      console.log("error:",err);
    });
  }
  /* Registro Google */
  googleLogin(){
    this.server.presentLoadingDefault("Ingresando desde Google");
    this.auth.doGoogleLogin().then((resp)=>{
      this.server.dismissLoading();
      this.navCtrl.navigateRoot("home");
      }).catch((err)=>{
      this.server.dismissLoading();
      this.server.showAlert("Error al Ingresar","Error al ingresar con Google, intente nuevamente.");
      console.log("error:",err);
    });
  }

}
