import { Component, ViewChild } from '@angular/core';
import { Platform, NavController, MenuController, LoadingController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AuthService } from './services/auth.service';
import { IntroPage } from './intro/intro.page';
import { HomePage } from './home/home.page';
import { EventEmitterService } from './services/events/event-emitter.service';
import { Subscription } from 'rxjs';

@Component({
    selector: 'app-root',
    templateUrl: 'app.component.html'
})
export class AppComponent {

  activePage: any;
  user;
  items: Array<{title: string, Component: any}>;
  nologin: boolean;
  listener: Subscription;
  public loading = null;

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private auth: AuthService,
    public nav: NavController,
    public menuCtrl: MenuController,
    public event: EventEmitterService,
    public loadingCtrl: LoadingController
  ) {
    this.initializeApp();
  }

  async createLoading() {
    this.loading = await this.loadingCtrl.create({
      spinner: "bubbles",
      message: "Cargando...",
      translucent: false,
      keyboardClose: true,
      duration: 8000
    });
    return await this.loading.present();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      console.log('App load sucessfull');
      if (this.platform.is('cordova') ) {
        this.statusBar.styleDefault();
        this.splashScreen.hide();
      }
      // this.platform.backButton.subscribe(() => {
      //   this.nav.pop();
      // });    
    });

    // Menu Data Builder
     const itemslogin = [
      {title: 'Home', Component: 'tabs/home'},
      {title: 'Mi Cuenta', Component: 'micuenta'},
      {title: 'Historial de Pedidos', Component: 'historial'},
      {title: 'Mis Direcciones', Component: 'mis-direcciones'},
      {title: 'Blog', Component: 'blog'},
      {title: 'Cerrar Sesión', Component: null}
    ];

    const itemsnologin = [
      {title: 'Home', Component: 'home'},
      {title: 'Blog', Component: 'blog'},
      {title: 'Ingresar', Component: 'log'},
      {title: 'Registrar', Component: 'registro'}
    ];

    this.listener = this.event.eventEmitter.subscribe(state => {
      switch (state) {
        case true:
          if(this.loading === null) this.createLoading();
          break;
        case false:
          if(this.loading !== null) {
            console.log("==> DISMISS");
            this.loading.dismiss();
            this.loading = null;
          }
          break;
      
        default:
          break;
      }
    });

    this.event.sendEmit();

    // Detect User Auth Login
    this.auth.afAuth.authState
    .subscribe(
        user => {
          if (user) {
            this.user = this.auth.getUser();
            this.nav.navigateRoot('tabs/home');
            this.items = itemslogin;
          } else {
            this.nav.navigateRoot('intro');
            this.nologin = true;
            this.items = itemsnologin;
          }
        },
        () => {
          this.user = null;
          this.nav.navigateRoot('intro');
        }
      );
    }

    openPage(page) {
      if (page) {
        this.nav.navigateForward(page);
        this.menuCtrl.toggle();
      } else {
         this.auth.signOut();
         this.nav.navigateRoot('intro');
      }
      this.activePage = page;
    }

    public checkActivePage(page): boolean {
      return page === this.activePage;
    }

  }
