import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';

@Injectable({
  providedIn: 'root'
})
export class SearchService {
	items: any;
  constructor() {
  	this.items = JSON.parse(localStorage.getItem('productos'));
  }
  filterItems(searchTerm){
  return this.items.filter((item) => {
   return item.nombreEs.toLowerCase().indexOf(
     searchTerm.toLowerCase()) > -1;
   });
  }
}
