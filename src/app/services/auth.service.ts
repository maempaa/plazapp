import { Injectable } from '@angular/core';
import { Platform } from '@ionic/angular';
import 'rxjs/add/operator/toPromise';

import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import AuthProvider = firebase.auth.AuthProvider;
import { firebaseConfig } from '../../config';

import { FirebaseUserModel } from "./user-model";

import { AngularFireDatabase } from '@angular/fire/database';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook/ngx';
import { GooglePlus } from '@ionic-native/google-plus/ngx';

import { ServerService } from "../server/server.service";
import { UserService } from '../services/user-service';



@Injectable({
  providedIn: 'root'
})

export class AuthService {
    private user: firebase.User;
    public endpoint = 'http://34.70.254.149';

	constructor(public afAuth: AngularFireAuth,
        public db: AngularFireDatabase,
        public fb: Facebook, 
        public googlePlus: GooglePlus,
        public platform: Platform,
        public server: ServerService,
        public userService: UserService) {
    		afAuth.authState.subscribe(user => {
          this.user = user;
          console.log("Datos de Usuario Actual:",user);
    		});
	}
    /* Varios */
    get authenticated(): boolean { return this.user !== null;}
    getUser(){ return this.user; }
    getEmail() { return this.user && this.user.email; }
    signOut(): Promise<void> { return this.afAuth.auth.signOut(); localStorage.setItem("phoneUser",''); }

    /* Loguear Email */
    doLogin(value){
      var me = this;
      return new Promise<any>((resolve, reject) => {
        firebase.auth().signInWithEmailAndPassword(value.email, value.password)
        .then(res => {
          this.userService.getCurrentUser()
          .then(user => {
          this.getGenKeyVal('Usuario','uid',user.uid).then(data => {
            localStorage.setItem("phoneUser",data[0].telefono);
          })
          })
          resolve(res);
        }, err => reject(err))
      })
    }
    /* Loguear Google */
    doGoogleLogin(){
     var me = this;
      return new Promise<FirebaseUserModel>((resolve, reject) => {
        if (this.platform.is('cordova') ) {
          this.googlePlus.login({
            'scopes': '', // optional, space-separated list of scopes, If not included or empty, defaults to `profile` and `email`.
            'webClientId': firebaseConfig.googleWebClientId, // optional clientId of your Web application from Credentials settings of your project - On Android, this MUST be included to get an idToken. On iOS, it is not required.
            'offline': true
          }).then((response) => {
            const googleCredential = firebase.auth.GoogleAuthProvider.credential(response.idToken);
            firebase.auth().signInAndRetrieveDataWithCredential(googleCredential)
            .then((user) => {
              me.server.createNewUser(false,"","");
              resolve();
            });
          },(err) => {
            reject(err);
          });
        }
        else{
          this.afAuth.auth
          .signInWithPopup(new firebase.auth.GoogleAuthProvider())
          .then((user) => {
            me.server.createNewUser(false,"","");
             resolve()
          },(err) => {
           reject(err);
         })
        }
      })
    }
    /* Loguear Facebook */
    doFacebookLogin(){
     var me = this;
     return new Promise<FirebaseUserModel>((resolve, reject) => {
       if (this.platform.is('cordova')) {
         this.fb.login(["public_profile","email"])
         .then((response) => {
           const facebookCredential = firebase.auth.FacebookAuthProvider.credential(response.authResponse.accessToken);
           firebase.auth().signInAndRetrieveDataWithCredential(facebookCredential)
            .then((user) => {
                me.server.createNewUser(false,"","");
                resolve()
            }).catch((err)=>reject(err));
         }, err => reject(err)
         );
       } else {
         this.afAuth.auth
         .signInWithPopup(new firebase.auth.FacebookAuthProvider())
         .then(result => {
           var bigImgUrl = "https://graph.facebook.com/" + result.additionalUserInfo.profile["id"] + "/picture?type=large";
           firebase.auth().currentUser.updateProfile({
             displayName: result.user.displayName,
             photoURL: bigImgUrl
           }).then((res) => {
            me.server.createNewUser(false,"","");
            resolve();
            }
           ,(err) => {
             reject(err);
           });
         },(err) => {
           reject(err);
         })
       }
     })
   }
   /* Registrar */
   doRegister(value){
    var me = this;
    return new Promise<any>((resolve, reject) => {
      firebase.auth().createUserWithEmailAndPassword(value.email, value.password)
      .then(res => {
        var user = firebase.auth().currentUser;
        user.updateProfile({
            displayName: value.username
        })
        me.server.createNewUser(true,value.username, value.tel);
        resolve(res);
      }, err => reject(err))
    })
   }
  /* Salir */
  doLogout(){
    return new Promise((resolve, reject) => {
      if(firebase.auth().currentUser){
        this.afAuth.auth.signOut()
        localStorage.setItem("phoneUser",'');
        resolve();
      }
      else {
        reject();
      }
    });
   }
  /* Reset Password */
  resetPassword(email: string) {
    var auth = firebase.auth();
    return auth.sendPasswordResetEmail(email)
      .then(() => console.log("email sent"))
      .catch((error) => console.log(error))
  }
  /* Get Firebase for Key */
    getGen(key) {
        return new Promise(resolve => {
            this.db.list<any[]>(key).valueChanges().subscribe(function(res){
            let pt = [];
            res.forEach(element => {
                pt.push(element);
            });
            resolve(pt);
            });
        });
    }
    /* Get Firebase for key and visible */
    getGenOV(key) {
        return new Promise(resolve => {
            this.db.list<any[]>(key, ref => ref.orderByChild('visible').equalTo(true)).valueChanges().subscribe(function(res){
            let pt = [];
            res.forEach(element => {
                pt.push(element);
            });
            pt= pt.sort((n1,n2)=> n2.Nombre - n1.orden);
            console.log(pt);
            resolve(pt);
            });
        });
    }
    /* Get Firebase for key and id */
    getGenKeyVal(tabla: string, field: any, id: any) {
      return new Promise(resolve => {
          this.db.list<any[]>(tabla, ref => ref.orderByChild(field).equalTo(id)).valueChanges().subscribe(function(res){
            let pt = [];
            res.forEach(element => {
              console.log(element);
                pt.push(element);
            });
            resolve(pt);
          });
      });
    }
}