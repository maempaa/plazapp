import { Component, OnInit } from '@angular/core';
import { NavController, LoadingController, AlertController, MenuController  } from '@ionic/angular';
import { Observable } from 'rxjs/Observable';
import { SearchService } from '../services/search.service';
import { FormControl } from '@angular/forms';
import 'rxjs/add/operator/debounceTime';
import { ParametrosService } from '../services/parametros.service';
import { Keyboard } from '@ionic-native/keyboard/ngx';

@Component({
  selector: 'app-buscar',
  templateUrl: './buscar.page.html',
  styleUrls: ['./buscar.page.scss'],
})
export class BuscarPage implements OnInit {

	searchTerm: string = '';
	searchControl: FormControl;
	items: any;
	searching: any = false;

  constructor(public navCtrl: NavController,public searchservice: SearchService,public param: ParametrosService, private menuCtrl: MenuController, private kb: Keyboard) {
  	this.searchControl = new FormControl();
  }

  ngOnInit() {
  }

  ionViewDidLoad() {
   	this.setFilteredItems();
   	this.searchControl.valueChanges.debounceTime(700).subscribe(search  => {
  		this.setFilteredItems();
  	});
  }

  setFilteredItems() {
    if (this.searchTerm=="") {
      this.items = [];
    } else {
      this.items = this.searchservice.filterItems(this.searchTerm);
    }
  }

  setFocus(){
    this.searching = true;
  }
  setBlur(){
    this.searching = false;
  }

  selectBuscar(id: any) {
    localStorage.setItem("fromSearch","true");
    localStorage.setItem("product-select",id);
    this.navCtrl.navigateForward("addproducto");
  }

  openMenu() {
    this.menuCtrl.toggle();
  }

}
