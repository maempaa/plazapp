import { Component, OnInit } from '@angular/core';
import { NavController, LoadingController, AlertController, MenuController  } from '@ionic/angular';
import { AuthService } from '../services/auth.service';
import { ServerService } from '../server/server.service';

@Component({
  selector: 'app-payload-program',
  templateUrl: './payload-program.page.html',
  styleUrls: ['./payload-program.page.scss'],
})
export class PayloadProgramPage implements OnInit {

	dia;
	ini;
	fini;
  min;
  max;
  hourvaluesini;
  hourvaluesfini;
  hourinterval;
  diashabiles;
  error = true;
  error_msg = '';

  constructor(public navCtrl: NavController,
    public menuCtrl: MenuController,
    public server: ServerService,
    private auth: AuthService) {
    var tzoffset = (new Date()).getTimezoneOffset() * 60000;
  	//this.dia = new Date(Date.now() - tzoffset).toISOString().slice(0, -1);
  	//this.ini = new Date(Date.now() - tzoffset).toISOString().slice(0, -1);
  	let now = new Date(Date.now() - tzoffset).getTime();
  	//let oneHoursIntoFuture = new Date(now + 1000 * 60 * 60 * 1);
  	//this.fini = oneHoursIntoFuture.toISOString().slice(0, -1);

    var date = new Date(), y = date.getFullYear(), m = date.getMonth(), d = date.getDay();
    var firstDate = new Date(y, m, 1);
    var lastDate = new Date(y, m + 1, 0);

    this.min = firstDate.toISOString().slice(0, -1);
    this.max = lastDate.toISOString().slice(0, -1);

    //Get setting for days
    this.auth.getGenKeyVal('Settings','llave','dias_habiles').then(data => {
      let indata: any;
      indata = data;
      this.diashabiles = indata[0].valor;
    })
    this.auth.getGenKeyVal('Settings','llave','tiempo_minimo').then(data => {
      let indata: any;
      indata = data;
      this.hourinterval = indata[0].valor;
    });
    this.auth.getGenKeyVal('Settings','llave','horas_habiles').then(data => {
      let indata: any;
      indata = data;
      let mininidata = indata[0].valor.split(',');

      let databuilder='';
      let mininidata2: any = parseInt(mininidata[0])+parseInt(this.hourinterval)-1;
      for (var i = mininidata2; i < mininidata[1]; i++) {
        databuilder += (i-1)+',';
        this.hourvaluesini = databuilder;
      }

      databuilder='';
      
      for (var i = mininidata2+1; i <= mininidata[1]; i++) {
        databuilder += i+',';
        this.hourvaluesfini = databuilder;
      }

      //console.log("Horas Habiles Ini",this.hourvaluesini)
      //console.log("Horas Habiles Fini",this.hourvaluesfini)

      //this.ini = new Date(new Date(y,m,d,mininidata[0],0,0).getTime()-tzoffset).toISOString().slice(0, -1);
      //this.fini = new Date(new Date(y,m,d,mininidata[1],0,0).getTime()-tzoffset).toISOString().slice(0, -1);
    });
  }

  ngOnInit() {
  }

  checkDate() {
    var tzoffset = (new Date()).getTimezoneOffset() * 60000;
    //let now = (new Date(Date.now() - tzoffset).getTime());
    //let dianow = (new Date(this.dia).getTime());
    /****/
    let diahoy = (new Date(Date.now() - tzoffset));
    let diaselect = new Date(this.dia+' '+diahoy.getHours()+':'+diahoy.getMinutes()+':'+diahoy.getSeconds()+':'+diahoy.getMilliseconds());
    /**/
    let houri = new Date(this.dia+' '+this.ini).getHours();
    let hourf = new Date(this.dia+' '+this.fini).getHours();
    let hourint = hourf-houri;
    /**/
    //console.log("Fecha diferencia",diaselect.getTime()-diahoy.getTime());
    if((diaselect.getTime()-diahoy.getTime())<0){
      this.error = true;
      this.error_msg = 'La fecha seleccionada es menor a la fecha actual.';
    } else if((diaselect.getTime()-diahoy.getTime())==0 && houri<diahoy.getUTCHours()) {
      this.error = true;
      this.error_msg = 'La hora inicial seleccionada para hoy es menor a la hora actual.';
    //} else if(isNaN(hourint)){
      //this.error = true;
      //this.error_msg = "La horas seleccionadas es menor al intervalo minimo: "+this.hourinterval+" Horas.";
    } else if((hourint)<this.hourinterval) {
      this.error = true;
      this.error_msg = 'La hora no han sido ajustadas';
    } else {
      this.error = false;
    }
  }

  continueCart(){
    if (this.error) {
      this.server.showAlert('Error de Programación',this.error_msg);
    } else {
      let range;
      console.log("diaaaa",this.dia);
      const hourdia = new Date(this.dia);
      console.log("new diaa",hourdia);
      const hourini = new Date(this.ini);
      const hourfini = new Date(this.fini);
      range = (hourdia.getDate()<10?'0':'') + hourdia.getDate() + '-'+ (hourdia.getMonth()+1<10?'0':'') + (hourdia.getMonth()+1) + '-' + hourdia.getFullYear() + '|' + hourini.getHours() + ':' + (hourini.getMinutes()<10?'0':'') + hourini.getMinutes() + '|' + hourfini.getHours() + ':' + (hourfini.getMinutes()<10?'0':'') + hourfini.getMinutes();
      localStorage.setItem('range-time', range);
    console.log('Range',range);
    this.navCtrl.navigateForward('payload-checkout');
    }
  }

  goBack() {
    this.navCtrl.navigateBack('tabs/cart');
  }


}
