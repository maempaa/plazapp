import { Component, OnInit } from '@angular/core';
import { NavController, LoadingController, AlertController, MenuController  } from '@ionic/angular';

@Component({
  selector: 'app-tipo-pago',
  templateUrl: './tipo-pago.page.html',
  styleUrls: ['./tipo-pago.page.scss'],
})
export class TipoPagoPage implements OnInit {

  tarjetaselected = 'Tarjeta Débito/Crédito';
  methodtipo;
	datamethod = [
		{checked: false},
		{checked: false},
    {checked: false},
    {checked: false}
	];

  constructor(public navCtrl: NavController,
    public menuCtrl: MenuController) {
    const methodactual = parseInt(localStorage.getItem('method'));
    console.log("metodo actual",methodactual);
  	if (!isNaN(methodactual)) {
  		this.datamethod[methodactual].checked = true;
      if (methodactual == 2) {
        if (localStorage.getItem("tarjeta_actual") === 'undefined' || localStorage.getItem("tarjeta_actual") === null){
          localStorage.setItem('method',"0");
          this.datamethod[0].checked = true;
        } else {
          const tarjetanombre = localStorage.getItem('tarjeta_nombre').split('|');
          this.tarjetaselected = 'XXXX-XXXX-XXXX-' + tarjetanombre[0];
          this.methodtipo = tarjetanombre[1];
        }
      }
  	}
  }

  ngOnInit() {
  }

  Selection(event: string) {
	   this.datamethod.forEach(x => { x.checked = false; });
     console.log('event', event);
      localStorage.setItem('method', event);
      if (event == '2') {
      localStorage.setItem('fromCart', 'true');
      this.navCtrl.navigateForward('tarjetas');
      } else {
      this.navCtrl.navigateBack('payload-checkout');
      }
	}

	goBack() {
    this.navCtrl.navigateBack('payload-checkout');
  }


}
