import { Component, OnInit } from '@angular/core';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { NavController, LoadingController, AlertController, MenuController  } from '@ionic/angular';
import { AuthService } from '../services/auth.service';


@Component({
  selector: 'app-contacto',
  templateUrl: './contacto.page.html',
  styleUrls: ['./contacto.page.scss'],
})
export class CONTACTOPage implements OnInit {

  chat_enable = "1";
  whatsapp = "Sin Whatsapp";
  email = "Sin Email";
  telefono = "Sin Télefono";

  constructor(private callNumber: CallNumber,
    public menuCtrl: MenuController, 
    private nav: NavController,
    private auth: AuthService) {}

  ngOnInit() {
  }

  ionViewWillEnter(){
    //Get setting for contact
    this.auth.getGenKeyVal("Settings","llave","chat_habilitado").then(data => {
      let indata: any;
      indata = data;
      this.chat_enable = indata[0].valor;
      console.log("Chat ",this.chat_enable);
    });
    this.auth.getGenKeyVal("Settings","llave","whatsapp_contacto").then(data => {
      let indata: any;
      indata = data;
      this.whatsapp = indata[0].valor;
      console.log("Whatsapp ",this.whatsapp);
    });
    this.auth.getGenKeyVal("Settings","llave","email_contacto").then(data => {
      let indata: any;
      indata = data;
      this.email = indata[0].valor;
      console.log("Email ",this.email);
    });
    this.auth.getGenKeyVal("Settings","llave","telefono_contacto").then(data => {
      let indata: any;
      indata = data;
      this.telefono = indata[0].valor;
      console.log("Telefono ",this.telefono);
    });
  }

  openPhone(phone: string){
  	this.callNumber.callNumber(phone, true)
	  .then(res => console.log('Launched dialer!', res))
	  .catch(err => console.log('Error launching dialer', err));
  }

  openWhatsapp(tel: String){
  	window.open("whatsapp://send?text=Hola,+solicito+soporte&phone="+tel,'_system')
  }

  openEmail(){
  	window.open('mailto:'+this.email, '_system');
  }

  openChat(){
    this.nav.navigateForward("chat");
  }

  openMenu() {
    this.menuCtrl.toggle();
  }

}
