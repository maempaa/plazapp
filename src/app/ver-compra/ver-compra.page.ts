import { Component, OnInit } from '@angular/core';
import { NavController, LoadingController, AlertController, MenuController  } from '@ionic/angular';
import { AuthService } from '../services/auth.service';
import { UserService } from '../services/user-service';
@Component({
  selector: 'app-ver-compra',
  templateUrl: './ver-compra.page.html',
  styleUrls: ['./ver-compra.page.scss'],
})
export class VerCompraPage implements OnInit {
  username;
  compra;
  total;
  status;
  idGen;
  num;
  evento;
  productos: any;
  metodo;
  fecha;
  comentarios: string = "";
  propina = 0;
  bolsa = "No";

  constructor(public navCtrl: NavController,
    public menuCtrl: MenuController,
    private user: UserService,
    private auth: AuthService,
    private alertCtrl: AlertController) { }

  ngOnInit() {
  }

  ionViewWillEnter(){
    //Get User
    let idcompra = parseInt(localStorage.getItem("idcompra"));
    this.getProducto(idcompra);
    //Get Products
  }

  goBack() {
    this.navCtrl.navigateBack("historial");
  }

  getProducto(id: number) {
    this.user.getCurrentUser()
    .then(user => {
      this.auth.getGenKeyVal("Compra","id",id).then(data => {
        this.compra = data;
        this.total = data[0].total;
        this.metodo = data[0].metodo;
        this.fecha = data[0].createdAt;
        this.propina = data[0].propina;
        this.comentarios = data[0].message;
        if(data[0].bolsa){
          this.bolsa = "Si";
        } else {
          this.bolsa = "No";
        }
        if (data[0].estado==4) {
        	this.status = "Compra Aprobada";
        }
        this.idGen = data[0].guid;
        let idcompra = data[0].id;
        this.auth.getGenKeyVal("Compraitem","compra",idcompra).then(data2 => {
          let datacompraitem: any = data2;
	        //this.productos = data2;
          for (let i = 0; i < datacompraitem.length; i++) {
            let producto = parseInt(datacompraitem[i].producto);
            this.auth.getGenKeyVal("Producto","id",producto).then(data3 => {
                datacompraitem[i].nombre = data3[0].nombreEs;
            });
            let talla = parseInt(datacompraitem[i].talla);
            this.auth.getGenKeyVal("Talla","id",talla).then(data3 => {
                datacompraitem[i].unidad = data3[0].nombreEs;
            });
          }
          this.productos = datacompraitem;
	      });
      })
    });
  }

  getinfoProducto(id: number){
    
  }

}
