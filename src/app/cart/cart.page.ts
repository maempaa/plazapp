import { Component, OnInit } from '@angular/core';
import { NavController, LoadingController, AlertController, MenuController  } from '@ionic/angular';
import { ParametrosService } from '../services/parametros.service';
import { ServerService } from '../server/server.service';

import { AuthService } from '../services/auth.service';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.page.html',
  styleUrls: ['./cart.page.scss'],
})
export class CartPage implements OnInit {
  tallas: any;
  cart: any;
  numcart: any;
  unitcart: any;
  maduracioncart: any;
  unittallacart: any;
  preciocart: any;
  tempproductos: any = [];
  productos: any;
  finalproductos: any = [];
  envio = 10000;
  total = 0;
  subtotal = 0;

  constructor(public navCtrl: NavController,
    public param: ParametrosService,
    public menuCtrl: MenuController,
    public server: ServerService,
    private auth: AuthService) { }

  ngOnInit() {

  }

  ionViewWillEnter() {
    // Get List of Tallas
    this.auth.getGenOV('Talla').then(data => {
      this.tallas = data;
      console.log('talas', this.tallas);
    });
    // Get setting for envio
    this.auth.getGenKeyVal('Settings','llave','valor_domicilio').then(data => {
      let indata: any;
      indata = data;
      console.log('envio', indata[0].valor);
      this.envio = parseInt(indata[0].valor);
      this.filterProductos();
      this.calculateTotal();
    });
    // Get carts values
    this.cart = JSON.parse(localStorage.getItem('cart'));
    this.numcart = JSON.parse(localStorage.getItem('numcart'));
    this.unitcart = JSON.parse(localStorage.getItem('unitcart'));
    this.unittallacart = JSON.parse(localStorage.getItem('unittallacart'));
    this.preciocart = JSON.parse(localStorage.getItem('preciocart'));
    this.maduracioncart = JSON.parse(localStorage.getItem('maduracioncart'));
    this.productos = JSON.parse(localStorage.getItem('productos'));

    console.log('unit', this.unitcart);
    console.log('num', this.numcart);

  }

  calculateTotal() {
    this.subtotal = 0;
    for (let i = 0; i < this.preciocart.length; i++) {
             this.subtotal = this.subtotal + parseInt(this.preciocart[i]);
    }
    console.log('subtotal+envio', this.subtotal + " - " + this.envio);
    this.total = this.subtotal + this.envio;
    localStorage.setItem('subtotal', String(this.subtotal));
    localStorage.setItem('envio', String(this.envio));
  }

  removeCart(index: any) {
  	console.log('id remove:', index);
  	this.cart = JSON.parse(localStorage.getItem('cart'));
    this.numcart = JSON.parse(localStorage.getItem('numcart'));
    this.unitcart = JSON.parse(localStorage.getItem('unitcart'));
    this.unittallacart = JSON.parse(localStorage.getItem('unittallacart'));
    this.preciocart = JSON.parse(localStorage.getItem('preciocart'));
    this.maduracioncart = JSON.parse(localStorage.getItem('maduracioncart'));
  	this.cart.splice(index, 1);
  	this.numcart.splice(index, 1);
    this.unitcart.splice(index, 1);
    this.unittallacart.splice(index, 1);
    this.preciocart.splice(index, 1);
    this.maduracioncart.splice(index, 1);
  	localStorage.setItem('cart', JSON.stringify(this.cart));
    localStorage.setItem('numcart', JSON.stringify(this.numcart));
    localStorage.setItem('unitcart', JSON.stringify(this.unitcart));
    localStorage.setItem('unittallacart', JSON.stringify(this.unittallacart));
    localStorage.setItem('preciocart', JSON.stringify(this.preciocart));
    localStorage.setItem('maduracioncart', JSON.stringify(this.maduracioncart));
    console.log('cart', this.cart);
    console.log('numcart', this.numcart);
    console.log('unitcart', this.unitcart);
    console.log('unittallacart', this.unittallacart);
    console.log('preciocart', this.preciocart);
    console.log('maduracioncart', this.maduracioncart);
    this.filterProductos();
    this.calculateTotal();
    this.reloadCart();
  }

  filterProductos() {
  	this.finalproductos = [];
    for (let i = 0; i < this.cart.length; i++) {
        this.finalproductos[i] = this.productos.filter(item => item.id === this.cart[i])[0];
    }
    console.log('cart', this.cart);
    console.log('nofilter', this.productos);
    console.log('filter', this.finalproductos);
    localStorage.setItem('badgecart', this.cart.length);
  }

  clearCart() {
  	this.finalproductos = [];
  	this.cart = [];
  	this.numcart = [];
    this.unitcart = [];
    this.unittallacart = [];
    this.preciocart = [];
    this.maduracioncart = [];
  	localStorage.setItem('cart', JSON.stringify(this.cart));
    localStorage.setItem('numcart', JSON.stringify(this.numcart));
    localStorage.setItem('unitcart', JSON.stringify(this.unitcart));
    localStorage.setItem('unittallacart', JSON.stringify(this.unittallacart));
    localStorage.setItem('preciocart', JSON.stringify(this.preciocart));
    localStorage.setItem('maduracioncart', JSON.stringify(this.maduracioncart));
    this.filterProductos();
    this.calculateTotal();
    this.reloadCart();
  }

  continueCart() {
    let phoneNumber = localStorage.phoneUser;
    if(!phoneNumber){
      this.navCtrl.navigateForward('micuenta');
      this.server.showAlert('Mensaje', "Para continuar con la compra debes ingresar tu número telefónico");
      return;
    }
    console.log(this.subtotal);
    if(this.subtotal <= 0)
    {
      this.removeCart(null);
      this.server.showAlert('Carrito sin productos', "No tienes ningún producto en el carrito.");
      return;
    }
    if (this.cart.length > 0) {
      this.navCtrl.navigateForward('payload-program');
    } else {
      this.server.showAlert('Carrito sin productos', "No tienes ningún producto en el carrito.");
    }
  }

  reloadCart() {
  	this.navCtrl.navigateForward('tabs/cart');
  }

  openMenu() {
    this.menuCtrl.toggle();
  }

  gotoproduct(id: any) {
    localStorage.setItem('product-select', id);
    this.navCtrl.navigateForward('addproducto');
  }

}
