import { Component, OnInit } from '@angular/core';
import { NavController, LoadingController, AlertController, MenuController  } from '@ionic/angular';
import { UserService } from '../services/user-service';

@Component({
  selector: 'app-payload-done',
  templateUrl: './payload-done.page.html',
  styleUrls: ['./payload-done.page.scss'],
})
export class PayloadDonePage implements OnInit {
	username;
	dir;
	range;
	nombredia = ["Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sábado"];

  constructor(public navCtrl: NavController,
    public menuCtrl: MenuController,
    private user: UserService,
    private alertCtrl: AlertController) { }

  ngOnInit() {
  }

  ionViewWillEnter(){
  	//Get User
  	this.user.getCurrentUser()
    .then(user => {
    	this.username = user.name;
    });
  	//Get Dir
  	let diractual = parseInt(localStorage.getItem("address"));
  	let dirs = JSON.parse(localStorage.getItem('list-address'));
  	let dirselect = dirs.filter(item => item.id === diractual)[0];
  	this.dir = dirselect.direccion;
  	//Get Range Time
    let rang = localStorage.getItem("range-time");
    let rangsplit = rang.split("|");
    let dia = new Date(rangsplit[0]);
    //let ini = new Date(rangsplit[1]).toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true });
    //let fini = new Date(rangsplit[2]).toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true });
    //this.range = this.nombredia[dia.getDay()]+" "+ini+" a "+fini;
    this.range = this.nombredia[dia.getDay()]+" "+rangsplit[1]+" a "+rangsplit[2];

      /*clean*/
      let cart = [];
      let numcart = [];
      let unitcart = [];
      let badgecart = "0";
      let preciocart = [];
      let maduracioncart = [];
      localStorage.setItem("cart",JSON.stringify(cart));
      localStorage.setItem("numcart",JSON.stringify(numcart));
      localStorage.setItem("unitcart",JSON.stringify(unitcart));
      localStorage.setItem("badgecart",badgecart);
      localStorage.setItem("preciocart",JSON.stringify(preciocart));
      localStorage.setItem("maduracioncart",JSON.stringify(maduracioncart));
  }

  goBack() {
    this.navCtrl.navigateRoot("tabs/home");
  }

}
