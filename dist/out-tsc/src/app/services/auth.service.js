import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { Platform } from '@ionic/angular';
import 'rxjs/add/operator/toPromise';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import { firebaseConfig } from '../../config';
import { AngularFireDatabase } from '@angular/fire/database';
import { Facebook } from '@ionic-native/facebook/ngx';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { ServerService } from "../server/server.service";
import { UserService } from '../services/user-service';
var AuthService = /** @class */ (function () {
    function AuthService(afAuth, db, fb, googlePlus, platform, server, userService) {
        var _this = this;
        this.afAuth = afAuth;
        this.db = db;
        this.fb = fb;
        this.googlePlus = googlePlus;
        this.platform = platform;
        this.server = server;
        this.userService = userService;
        this.endpoint = 'http://34.70.254.149';
        console.log('AuthService Start');
        afAuth.authState.subscribe(function (user) {
            _this.user = user;
            console.log("User Data Service:", user);
        });
    }
    AuthService.prototype.checkDatosLoc = function () {
        this.checkDatos(this.user);
    };
    AuthService.prototype.checkDatos = function (data) {
        if (data) {
            var my_db_1 = this.db;
            this.db.list('users_listapp', function (ref) { return ref.orderByChild('email').equalTo(data.email); }).valueChanges().subscribe(function (res) {
                console.log(res);
                var users = res;
                if (users.length > 0) {
                }
                else {
                    var datos = { "email": data.email, "displayName": data.displayName, "Telefono": data.telefono, "photoURL": '' };
                    my_db_1.list('users_listapp' + '/').push(datos).then(function (res) {
                        console.log(res);
                    });
                }
            });
        }
    };
    AuthService.prototype.checkDatosEP = function (data) {
        if (data) {
            var my_db_2 = this.db;
            this.db.list('users_listapp', function (ref) { return ref.orderByChild('email').equalTo(data.email); }).valueChanges().subscribe(function (res) {
                console.log(res);
                var users = res;
                if (users.length > 0) {
                }
                else {
                    var datos = { "email": data.email, "displayName": data.displayName, "Telefono": '', "photoURL": data.photoURL };
                    my_db_2.list('users_listapp' + '/').push(datos).then(function (res) {
                        console.log(res);
                    });
                }
            });
        }
    };
    Object.defineProperty(AuthService.prototype, "authenticated", {
        get: function () { return this.user !== null; },
        enumerable: true,
        configurable: true
    });
    AuthService.prototype.getUser = function () { return this.user; };
    AuthService.prototype.getEmail = function () { return this.user && this.user.email; };
    AuthService.prototype.signOut = function () { return this.afAuth.auth.signOut(); localStorage.setItem("phoneUser", ''); };
    AuthService.prototype.doLogin = function (value) {
        var _this = this;
        var me = this;
        return new Promise(function (resolve, reject) {
            firebase.auth().signInWithEmailAndPassword(value.email, value.password)
                .then(function (res) {
                _this.userService.getCurrentUser()
                    .then(function (user) {
                    _this.getGenKeyVal('Usuario', 'uid', user.uid).then(function (data) {
                        localStorage.setItem("phoneUser", data[0].telefono);
                    });
                });
                resolve(res);
            }, function (err) { return reject(err); });
        });
    };
    AuthService.prototype.doGoogleLogin = function () {
        var _this = this;
        var me = this;
        return new Promise(function (resolve, reject) {
            if (_this.platform.is('cordova')) {
                console.log("en google cordova");
                _this.googlePlus.login({
                    'scopes': '',
                    'webClientId': firebaseConfig.googleWebClientId,
                    'offline': true
                }).then(function (response) {
                    console.log("respues de google", response);
                    var googleCredential = firebase.auth.GoogleAuthProvider.credential(response.idToken);
                    firebase.auth().signInAndRetrieveDataWithCredential(googleCredential)
                        .then(function (user) {
                        console.log("antes de crear usuario");
                        me.server.createNewUser(false, "", "");
                        resolve();
                    });
                }, function (err) {
                    console.log("error de google", err);
                    reject(err);
                });
            }
            else {
                console.log("sin cordova");
                _this.afAuth.auth
                    .signInWithPopup(new firebase.auth.GoogleAuthProvider())
                    .then(function (user) {
                    console.log("antes de crear usuario");
                    me.server.createNewUser(false, "", "");
                    resolve();
                }, function (err) {
                    reject(err);
                });
            }
        });
    };
    AuthService.prototype.doFacebookLogin = function () {
        var _this = this;
        var me = this;
        return new Promise(function (resolve, reject) {
            if (_this.platform.is('cordova')) {
                _this.fb.login(["public_profile"])
                    .then(function (response) {
                    console.log("Response From Facebook", response);
                    var facebookCredential = firebase.auth.FacebookAuthProvider.credential(response.authResponse.accessToken);
                    console.log("facebook Credentials", facebookCredential);
                    firebase.auth().signInAndRetrieveDataWithCredential(facebookCredential)
                        .then(function (user) {
                        console.log("Facebook User Received", user);
                        me.server.createNewUser(false, "", "");
                        resolve();
                    }).catch(function (err) { return reject(err); });
                }, function (err) { return reject(err); });
            }
            else {
                _this.afAuth.auth
                    .signInWithPopup(new firebase.auth.FacebookAuthProvider())
                    .then(function (result) {
                    var bigImgUrl = "https://graph.facebook.com/" + result.additionalUserInfo.profile["id"] + "/picture?type=large";
                    firebase.auth().currentUser.updateProfile({
                        displayName: result.user.displayName,
                        photoURL: bigImgUrl
                    }).then(function (res) {
                        me.server.createNewUser(false, "", "");
                        resolve();
                    }, function (err) {
                        reject(err);
                    });
                }, function (err) {
                    reject(err);
                });
            }
        });
    };
    AuthService.prototype.doRegister = function (value) {
        var me = this;
        return new Promise(function (resolve, reject) {
            firebase.auth().createUserWithEmailAndPassword(value.email, value.password)
                .then(function (res) {
                var user = firebase.auth().currentUser;
                user.updateProfile({
                    displayName: value.username
                });
                me.server.createNewUser(true, value.username, value.tel);
                resolve(res);
            }, function (err) { return reject(err); });
        });
    };
    AuthService.prototype.doLogout = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            if (firebase.auth().currentUser) {
                _this.afAuth.auth.signOut();
                localStorage.setItem("phoneUser", '');
                resolve();
            }
            else {
                reject();
            }
        });
    };
    AuthService.prototype.resetPassword = function (email) {
        var auth = firebase.auth();
        return auth.sendPasswordResetEmail(email)
            .then(function () { return console.log("email sent"); })
            .catch(function (error) { return console.log(error); });
    };
    AuthService.prototype.getGen = function (key) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.db.list(key).valueChanges().subscribe(function (res) {
                var pt = [];
                res.forEach(function (element) {
                    pt.push(element);
                });
                resolve(pt);
            });
        });
    };
    AuthService.prototype.getGenOV = function (key) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.db.list(key, function (ref) { return ref.orderByChild('visible').equalTo(true); }).valueChanges().subscribe(function (res) {
                var pt = [];
                res.forEach(function (element) {
                    pt.push(element);
                });
                pt = pt.sort(function (n1, n2) { return n2.orden - n1.orden; });
                resolve(pt);
            });
        });
    };
    AuthService.prototype.getGenKeyVal = function (tabla, field, id) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.db.list(tabla, function (ref) { return ref.orderByChild(field).equalTo(id); }).valueChanges().subscribe(function (res) {
                var pt = [];
                res.forEach(function (element) {
                    console.log(element);
                    pt.push(element);
                });
                resolve(pt);
            });
        });
    };
    AuthService = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [AngularFireAuth,
            AngularFireDatabase,
            Facebook,
            GooglePlus,
            Platform,
            ServerService,
            UserService])
    ], AuthService);
    return AuthService;
}());
export { AuthService };
//# sourceMappingURL=auth.service.js.map