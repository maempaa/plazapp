import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
var SearchService = /** @class */ (function () {
    function SearchService() {
        this.items = JSON.parse(localStorage.getItem('productos'));
    }
    SearchService.prototype.filterItems = function (searchTerm) {
        return this.items.filter(function (item) {
            return item.nombreEs.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1;
        });
    };
    SearchService = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [])
    ], SearchService);
    return SearchService;
}());
export { SearchService };
//# sourceMappingURL=search.service.js.map