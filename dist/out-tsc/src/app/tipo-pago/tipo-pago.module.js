import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { TipoPagoPage } from './tipo-pago.page';
var routes = [
    {
        path: '',
        component: TipoPagoPage
    }
];
var TipoPagoPageModule = /** @class */ (function () {
    function TipoPagoPageModule() {
    }
    TipoPagoPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [TipoPagoPage]
        })
    ], TipoPagoPageModule);
    return TipoPagoPageModule;
}());
export { TipoPagoPageModule };
//# sourceMappingURL=tipo-pago.module.js.map