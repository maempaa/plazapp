import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { MenuController } from '@ionic/angular';
var HistorialPage = /** @class */ (function () {
    function HistorialPage(menuCtrl) {
        this.menuCtrl = menuCtrl;
    }
    HistorialPage.prototype.ngOnInit = function () {
    };
    HistorialPage.prototype.openMenu = function () {
        this.menuCtrl.toggle();
    };
    HistorialPage.prototype.ayuda = function () {
        window.open('mailto:plaz@gmail.com', "_system");
    };
    HistorialPage = tslib_1.__decorate([
        Component({
            selector: 'app-historial',
            templateUrl: './historial.page.html',
            styleUrls: ['./historial.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [MenuController])
    ], HistorialPage);
    return HistorialPage;
}());
export { HistorialPage };
//# sourceMappingURL=historial.page.js.map