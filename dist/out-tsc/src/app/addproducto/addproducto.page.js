import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';
import { ParametrosService } from '../services/parametros.service';
import { AuthService } from '../services/auth.service';
var AddproductoPage = /** @class */ (function () {
    function AddproductoPage(navCtrl, auth, param) {
        this.navCtrl = navCtrl;
        this.auth = auth;
        this.param = param;
        this.num = 1;
        this.unit = "1";
        this.precio_producto = "0";
        this.cartcolor = "primary";
        this.carttext = "Agregar al carrito";
        this.estadoproducto = "Normal";
        this.checkboxs = [
            { val: 'Verde', isChecked: false },
            { val: 'Normal', isChecked: true },
            { val: 'Maduro', isChecked: false }
        ];
    }
    AddproductoPage.prototype.ngOnInit = function () {
    };
    AddproductoPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        //Get ID product
        this.param.serviceData
            .subscribe(function (product) { return (_this.productoid = product); });
        //Get list of units
        this.auth.getGenOV("Talla").then(function (data) {
            _this.tallas = data;
        });
        //Get list from Inventario
        this.auth.getGenKeyVal("Inventario", "producto", this.productoid).then(function (data) {
            _this.inventario = data;
            _this.precio_producto = _this.inventario[0].precio;
            _this.preciop = parseInt(_this.inventario[0].precio);
        });
        //Filter
        this.producto = JSON.parse(localStorage.getItem('productos'));
        this.producto = this.producto.filter(function (item) { return item.id === _this.productoid; });
        localStorage.setItem("producto-actual", JSON.stringify(this.producto));
        //Detect if product was in cart
        var cart = JSON.parse(localStorage.getItem('cart'));
        if (cart.indexOf(this.productoid) == -1) {
            this.cartcolor = "primary";
            this.carttext = "Agregar al carrito";
        }
        else {
            this.cartcolor = "danger";
            this.carttext = "Agregado al carrito! ";
        }
    };
    AddproductoPage.prototype.Selection = function (item) {
        this.checkboxs.forEach(function (x) { x.isChecked = false; });
        this.estadoproducto = item.val;
    };
    AddproductoPage.prototype.nameTalla = function (id) {
        var talla = this.tallas.filter(function (item) { return item.id === id; });
        return talla[0].nombreEs;
    };
    AddproductoPage.prototype.onUnitChange = function (id) {
        var idp = parseInt(id);
        var precio = this.inventario.filter(function (item) { return item.id === idp; });
        console.log("precio", precio);
        this.preciop = parseInt(precio[0].precio);
        var numberp = parseInt(this.num);
        var totalp = this.preciop * numberp;
        this.precio_producto = String(totalp);
    };
    AddproductoPage.prototype.onNumChange = function (num) {
        var nump = parseInt(num);
        var totalp = this.preciop * nump;
        this.precio_producto = String(totalp);
    };
    AddproductoPage.prototype.goRecetas = function (id) {
        this.param.dataProducto(id);
        this.navCtrl.navigateForward("recetas");
    };
    AddproductoPage.prototype.goProp = function (id) {
        this.param.dataProducto(id);
        this.navCtrl.navigateForward("propiedades");
    };
    AddproductoPage.prototype.addCart = function (id) {
        console.log("num:", this.num);
        console.log("unit:", this.unit);
        console.log("precio:", this.preciop);
        var cart = JSON.parse(localStorage.getItem('cart'));
        var numcart = JSON.parse(localStorage.getItem('numcart'));
        var unitcart = JSON.parse(localStorage.getItem('unitcart'));
        var preciocart = JSON.parse(localStorage.getItem('preciocart'));
        if (cart.indexOf(id) == -1) {
            cart.push(id);
            numcart.push(this.num);
            unitcart.push(this.unit);
            preciocart.push(this.precio_producto);
            localStorage.setItem("cart", JSON.stringify(cart));
            localStorage.setItem("numcart", JSON.stringify(numcart));
            localStorage.setItem("unitcart", JSON.stringify(unitcart));
            localStorage.setItem("preciocart", JSON.stringify(preciocart));
            this.cartcolor = "danger";
            this.carttext = "Agregado al carrito";
        }
        console.log("cart", cart);
        console.log("numcart", numcart);
        console.log("unitcart", unitcart);
        console.log("preciocart", preciocart);
        localStorage.setItem("badgecart", cart.length);
        this.goBack();
    };
    AddproductoPage.prototype.goBack = function () {
        var from = localStorage.getItem("fromSearch");
        if (from == "true") {
            this.navCtrl.navigateBack("tabs/buscar");
        }
        else {
            this.navCtrl.navigateBack("tabs/shop");
        }
    };
    AddproductoPage = tslib_1.__decorate([
        Component({
            selector: 'app-addproducto',
            templateUrl: './addproducto.page.html',
            styleUrls: ['./addproducto.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [NavController,
            AuthService,
            ParametrosService])
    ], AddproductoPage);
    return AddproductoPage;
}());
export { AddproductoPage };
//# sourceMappingURL=addproducto.page.js.map